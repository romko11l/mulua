# μLua - интерпретатор простого модельного языка

## Основные модули:

- `internal/parser` - Парсер, сгенерированный с помощью antlr4 по грамматике `Mlua.g4`
- `internal/runtime` - Правила интерпретации программы по её AST-дереву

-----------------------------

## HowTo:

- `make build` - сборка бинарного файла
- `make build-deb` - сборка deb пакета
- `./bin/mlua lex ./path/to/program` - просмотр лексем программы
- `./bin/mlua ast ./path/to/program` - просмотр AST-дерева программы
- `./bin/mlua run ./path/to/program` - запуск программы

-----------------------------

## Requirements:

- `go 1.16`
- `antlr 4.7.2`