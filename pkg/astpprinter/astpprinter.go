package astpprinter

import "fmt"

func PrintAst(ast string) {
	indent := ""
	for _, symb := range ast {
		switch symb {
		case '(':
			indent += "  "
			fmt.Printf("\n%s%c", indent, symb)
		case ')':
			indent = indent[:len(indent)-2]
			fmt.Printf("%c", symb)
		default:
			fmt.Printf("%c", symb)
		}
	}
	fmt.Printf("\n\n")
}
