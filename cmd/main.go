package main

import (
	"fmt"
	"os"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/romko11l/mulua/internal/parser"
	"gitlab.com/romko11l/mulua/internal/runtime"
	"gitlab.com/romko11l/mulua/pkg/astpprinter"
)

func run() error {
	data, err := os.ReadFile(os.Args[2])
	if err != nil {
		return err
	}
	is := antlr.NewInputStream(string(data))
	lexer := parser.NewMluaLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	p := parser.NewMluaParser(stream)
	tree := p.Program()
	visitor := runtime.NewMluaVisitor()
	visitor.VisitProgram(tree.(*parser.ProgramContext))
	return nil
}

func ast() error {
	data, err := os.ReadFile(os.Args[2])
	if err != nil {
		return err
	}
	is := antlr.NewInputStream(string(data))
	lexer := parser.NewMluaLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	p := parser.NewMluaParser(stream)
	tree := p.Program()
	astpprinter.PrintAst(tree.(*parser.ProgramContext).ToStringTree(lexer.RuleNames, p))
	return nil
}

func lex() error {
	data, err := os.ReadFile(os.Args[2])
	if err != nil {
		return err
	}
	is := antlr.NewInputStream(string(data))
	lexer := parser.NewMluaLexer(is)
	for {
		t := lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		fmt.Printf("%s (%q)\n", lexer.SymbolicNames[t.GetTokenType()], t.GetText())
	}
	return nil
}

func printHelpInfo() {
	fmt.Println("Mlua is a simple interpreter.\n\nUsage:\n    mlua [lex|ast|run] /path/to/file")
}

func main() {
	if len(os.Args) < 2 {
		printHelpInfo()
		return
	}

	command := os.Args[1]
	switch command {
	case "run":
		err := run()
		if err != nil {
			printHelpInfo()
		}
	case "ast":
		err := ast()
		if err != nil {
			printHelpInfo()
		}
	case "lex":
		err := lex()
		if err != nil {
			printHelpInfo()
		}
	default:
		printHelpInfo()
	}
}
