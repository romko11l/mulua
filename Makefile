build:
	go build -o ./bin/mlua ./cmd/.

.PHONY: build

grammar:
	antlr4 -Dlanguage=Go -visitor -no-listener ./internal/parser/Mlua.g4

.PHONY: grammar	

rebuild: grammar build

.PHONY: rebuild

build-deb: build
	mkdir -p ./mlua/usr/bin
	cp ./bin/mlua ./mlua/usr/bin/
	chmod 0755 ./mlua/DEBIAN
	dpkg-deb --build ./mlua

.PHONY: build-deb

lint: 
	golangci-lint -c .golangci.yml run

.PRHONY: lint