// Code generated from ./internal/parser/Mlua.g4 by ANTLR 4.7.2. DO NOT EDIT.

package parser // Mlua

import "github.com/antlr/antlr4/runtime/Go/antlr"

type BaseMluaVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseMluaVisitor) VisitProgram(ctx *ProgramContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitIdenitfierExpression(ctx *IdenitfierExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitParenthesizedExpression(ctx *ParenthesizedExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryComparisonExpression(ctx *BinaryComparisonExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitLiteralExpression(ctx *LiteralExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitUnaryExpression(ctx *UnaryExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitInputExpression(ctx *InputExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryMulDivExpression(ctx *BinaryMulDivExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryAddSubExpression(ctx *BinaryAddSubExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryMulDivOperator(ctx *BinaryMulDivOperatorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryAddSubOperator(ctx *BinaryAddSubOperatorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBinaryComparisonOperator(ctx *BinaryComparisonOperatorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitPrintStatement(ctx *PrintStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitAssignmentStatement(ctx *AssignmentStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitIfStatement(ctx *IfStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitWhileStatement(ctx *WhileStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitStatement(ctx *StatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseMluaVisitor) VisitBlockStatement(ctx *BlockStatementContext) interface{} {
	return v.VisitChildren(ctx)
}
