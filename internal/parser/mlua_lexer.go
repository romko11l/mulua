// Code generated from ./internal/parser/Mlua.g4 by ANTLR 4.7.2. DO NOT EDIT.

package parser

import (
	"fmt"
	"unicode"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = unicode.IsLetter

var serializedLexerAtn = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 2, 29, 174,
	8, 1, 4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7,
	9, 7, 4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12,
	4, 13, 9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4,
	18, 9, 18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23,
	9, 23, 4, 24, 9, 24, 4, 25, 9, 25, 4, 26, 9, 26, 4, 27, 9, 27, 4, 28, 9,
	28, 4, 29, 9, 29, 4, 30, 9, 30, 3, 2, 3, 2, 3, 3, 3, 3, 3, 4, 3, 4, 3,
	4, 3, 4, 3, 5, 3, 5, 3, 5, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3,
	6, 3, 7, 3, 7, 3, 7, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 9, 3, 9, 3, 9, 3,
	9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 11, 3, 11, 3, 11,
	3, 12, 3, 12, 3, 12, 3, 12, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3,
	14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 15, 3, 15, 3, 16, 3, 16, 3, 17,
	3, 17, 3, 18, 3, 18, 3, 19, 3, 19, 3, 20, 3, 20, 3, 20, 3, 21, 3, 21, 3,
	21, 3, 22, 3, 22, 3, 22, 3, 23, 3, 23, 3, 24, 3, 24, 3, 24, 3, 25, 3, 25,
	3, 26, 3, 26, 3, 26, 3, 27, 3, 27, 3, 28, 6, 28, 151, 10, 28, 13, 28, 14,
	28, 152, 3, 28, 6, 28, 156, 10, 28, 13, 28, 14, 28, 157, 3, 28, 5, 28,
	161, 10, 28, 3, 29, 6, 29, 164, 10, 29, 13, 29, 14, 29, 165, 3, 30, 6,
	30, 169, 10, 30, 13, 30, 14, 30, 170, 3, 30, 3, 30, 2, 2, 31, 3, 2, 5,
	2, 7, 3, 9, 4, 11, 5, 13, 6, 15, 7, 17, 8, 19, 9, 21, 10, 23, 11, 25, 12,
	27, 13, 29, 14, 31, 15, 33, 16, 35, 17, 37, 18, 39, 19, 41, 20, 43, 21,
	45, 22, 47, 23, 49, 24, 51, 25, 53, 26, 55, 27, 57, 28, 59, 29, 3, 2, 5,
	3, 2, 50, 59, 4, 2, 67, 92, 99, 124, 5, 2, 11, 12, 15, 15, 34, 34, 2, 176,
	2, 7, 3, 2, 2, 2, 2, 9, 3, 2, 2, 2, 2, 11, 3, 2, 2, 2, 2, 13, 3, 2, 2,
	2, 2, 15, 3, 2, 2, 2, 2, 17, 3, 2, 2, 2, 2, 19, 3, 2, 2, 2, 2, 21, 3, 2,
	2, 2, 2, 23, 3, 2, 2, 2, 2, 25, 3, 2, 2, 2, 2, 27, 3, 2, 2, 2, 2, 29, 3,
	2, 2, 2, 2, 31, 3, 2, 2, 2, 2, 33, 3, 2, 2, 2, 2, 35, 3, 2, 2, 2, 2, 37,
	3, 2, 2, 2, 2, 39, 3, 2, 2, 2, 2, 41, 3, 2, 2, 2, 2, 43, 3, 2, 2, 2, 2,
	45, 3, 2, 2, 2, 2, 47, 3, 2, 2, 2, 2, 49, 3, 2, 2, 2, 2, 51, 3, 2, 2, 2,
	2, 53, 3, 2, 2, 2, 2, 55, 3, 2, 2, 2, 2, 57, 3, 2, 2, 2, 2, 59, 3, 2, 2,
	2, 3, 61, 3, 2, 2, 2, 5, 63, 3, 2, 2, 2, 7, 65, 3, 2, 2, 2, 9, 69, 3, 2,
	2, 2, 11, 74, 3, 2, 2, 2, 13, 80, 3, 2, 2, 2, 15, 83, 3, 2, 2, 2, 17, 88,
	3, 2, 2, 2, 19, 93, 3, 2, 2, 2, 21, 99, 3, 2, 2, 2, 23, 102, 3, 2, 2, 2,
	25, 106, 3, 2, 2, 2, 27, 112, 3, 2, 2, 2, 29, 118, 3, 2, 2, 2, 31, 120,
	3, 2, 2, 2, 33, 122, 3, 2, 2, 2, 35, 124, 3, 2, 2, 2, 37, 126, 3, 2, 2,
	2, 39, 128, 3, 2, 2, 2, 41, 131, 3, 2, 2, 2, 43, 134, 3, 2, 2, 2, 45, 137,
	3, 2, 2, 2, 47, 139, 3, 2, 2, 2, 49, 142, 3, 2, 2, 2, 51, 144, 3, 2, 2,
	2, 53, 147, 3, 2, 2, 2, 55, 150, 3, 2, 2, 2, 57, 163, 3, 2, 2, 2, 59, 168,
	3, 2, 2, 2, 61, 62, 9, 2, 2, 2, 62, 4, 3, 2, 2, 2, 63, 64, 9, 3, 2, 2,
	64, 6, 3, 2, 2, 2, 65, 66, 7, 112, 2, 2, 66, 67, 7, 107, 2, 2, 67, 68,
	7, 110, 2, 2, 68, 8, 3, 2, 2, 2, 69, 70, 7, 118, 2, 2, 70, 71, 7, 116,
	2, 2, 71, 72, 7, 119, 2, 2, 72, 73, 7, 103, 2, 2, 73, 10, 3, 2, 2, 2, 74,
	75, 7, 104, 2, 2, 75, 76, 7, 99, 2, 2, 76, 77, 7, 110, 2, 2, 77, 78, 7,
	117, 2, 2, 78, 79, 7, 103, 2, 2, 79, 12, 3, 2, 2, 2, 80, 81, 7, 107, 2,
	2, 81, 82, 7, 104, 2, 2, 82, 14, 3, 2, 2, 2, 83, 84, 7, 103, 2, 2, 84,
	85, 7, 110, 2, 2, 85, 86, 7, 117, 2, 2, 86, 87, 7, 103, 2, 2, 87, 16, 3,
	2, 2, 2, 88, 89, 7, 118, 2, 2, 89, 90, 7, 106, 2, 2, 90, 91, 7, 103, 2,
	2, 91, 92, 7, 112, 2, 2, 92, 18, 3, 2, 2, 2, 93, 94, 7, 121, 2, 2, 94,
	95, 7, 106, 2, 2, 95, 96, 7, 107, 2, 2, 96, 97, 7, 110, 2, 2, 97, 98, 7,
	103, 2, 2, 98, 20, 3, 2, 2, 2, 99, 100, 7, 102, 2, 2, 100, 101, 7, 113,
	2, 2, 101, 22, 3, 2, 2, 2, 102, 103, 7, 103, 2, 2, 103, 104, 7, 112, 2,
	2, 104, 105, 7, 102, 2, 2, 105, 24, 3, 2, 2, 2, 106, 107, 7, 107, 2, 2,
	107, 108, 7, 112, 2, 2, 108, 109, 7, 114, 2, 2, 109, 110, 7, 119, 2, 2,
	110, 111, 7, 118, 2, 2, 111, 26, 3, 2, 2, 2, 112, 113, 7, 114, 2, 2, 113,
	114, 7, 116, 2, 2, 114, 115, 7, 107, 2, 2, 115, 116, 7, 112, 2, 2, 116,
	117, 7, 118, 2, 2, 117, 28, 3, 2, 2, 2, 118, 119, 7, 42, 2, 2, 119, 30,
	3, 2, 2, 2, 120, 121, 7, 43, 2, 2, 121, 32, 3, 2, 2, 2, 122, 123, 7, 45,
	2, 2, 123, 34, 3, 2, 2, 2, 124, 125, 7, 47, 2, 2, 125, 36, 3, 2, 2, 2,
	126, 127, 7, 44, 2, 2, 127, 38, 3, 2, 2, 2, 128, 129, 7, 49, 2, 2, 129,
	130, 7, 49, 2, 2, 130, 40, 3, 2, 2, 2, 131, 132, 7, 63, 2, 2, 132, 133,
	7, 63, 2, 2, 133, 42, 3, 2, 2, 2, 134, 135, 7, 128, 2, 2, 135, 136, 7,
	63, 2, 2, 136, 44, 3, 2, 2, 2, 137, 138, 7, 62, 2, 2, 138, 46, 3, 2, 2,
	2, 139, 140, 7, 62, 2, 2, 140, 141, 7, 63, 2, 2, 141, 48, 3, 2, 2, 2, 142,
	143, 7, 64, 2, 2, 143, 50, 3, 2, 2, 2, 144, 145, 7, 64, 2, 2, 145, 146,
	7, 63, 2, 2, 146, 52, 3, 2, 2, 2, 147, 148, 7, 63, 2, 2, 148, 54, 3, 2,
	2, 2, 149, 151, 5, 5, 3, 2, 150, 149, 3, 2, 2, 2, 151, 152, 3, 2, 2, 2,
	152, 150, 3, 2, 2, 2, 152, 153, 3, 2, 2, 2, 153, 160, 3, 2, 2, 2, 154,
	156, 5, 3, 2, 2, 155, 154, 3, 2, 2, 2, 156, 157, 3, 2, 2, 2, 157, 155,
	3, 2, 2, 2, 157, 158, 3, 2, 2, 2, 158, 161, 3, 2, 2, 2, 159, 161, 3, 2,
	2, 2, 160, 155, 3, 2, 2, 2, 160, 159, 3, 2, 2, 2, 161, 56, 3, 2, 2, 2,
	162, 164, 5, 3, 2, 2, 163, 162, 3, 2, 2, 2, 164, 165, 3, 2, 2, 2, 165,
	163, 3, 2, 2, 2, 165, 166, 3, 2, 2, 2, 166, 58, 3, 2, 2, 2, 167, 169, 9,
	4, 2, 2, 168, 167, 3, 2, 2, 2, 169, 170, 3, 2, 2, 2, 170, 168, 3, 2, 2,
	2, 170, 171, 3, 2, 2, 2, 171, 172, 3, 2, 2, 2, 172, 173, 8, 30, 2, 2, 173,
	60, 3, 2, 2, 2, 8, 2, 152, 157, 160, 165, 170, 3, 8, 2, 2,
}

var lexerDeserializer = antlr.NewATNDeserializer(nil)
var lexerAtn = lexerDeserializer.DeserializeFromUInt16(serializedLexerAtn)

var lexerChannelNames = []string{
	"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
}

var lexerModeNames = []string{
	"DEFAULT_MODE",
}

var lexerLiteralNames = []string{
	"", "'nil'", "'true'", "'false'", "'if'", "'else'", "'then'", "'while'",
	"'do'", "'end'", "'input'", "'print'", "'('", "')'", "'+'", "'-'", "'*'",
	"'//'", "'=='", "'~='", "'<'", "'<='", "'>'", "'>='", "'='",
}

var lexerSymbolicNames = []string{
	"", "NilLiteral", "TrueLiteral", "FalseLiteral", "IfLiteral", "ElseLiteral",
	"ThenLiteral", "WhileLiteral", "DoLiteral", "EndLiteral", "InputLiteral",
	"PrintLiteral", "LparenLiteral", "RparenLiteral", "PlusLiteral", "MinusLiteral",
	"MultiplicationLiteral", "DivisionLiteral", "DoubleEqualLiteral", "NotEqualLiteral",
	"LessLiteral", "LessOrEqualLiteral", "GreaterLiteral", "GreaterOrEqualLiteral",
	"EqualLiteral", "Idenitfier", "IntegerLiteral", "WHITESPACE",
}

var lexerRuleNames = []string{
	"DIGIT", "LETTER", "NilLiteral", "TrueLiteral", "FalseLiteral", "IfLiteral",
	"ElseLiteral", "ThenLiteral", "WhileLiteral", "DoLiteral", "EndLiteral",
	"InputLiteral", "PrintLiteral", "LparenLiteral", "RparenLiteral", "PlusLiteral",
	"MinusLiteral", "MultiplicationLiteral", "DivisionLiteral", "DoubleEqualLiteral",
	"NotEqualLiteral", "LessLiteral", "LessOrEqualLiteral", "GreaterLiteral",
	"GreaterOrEqualLiteral", "EqualLiteral", "Idenitfier", "IntegerLiteral",
	"WHITESPACE",
}

type MluaLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var lexerDecisionToDFA = make([]*antlr.DFA, len(lexerAtn.DecisionToState))

func init() {
	for index, ds := range lexerAtn.DecisionToState {
		lexerDecisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

func NewMluaLexer(input antlr.CharStream) *MluaLexer {

	l := new(MluaLexer)

	l.BaseLexer = antlr.NewBaseLexer(input)
	l.Interpreter = antlr.NewLexerATNSimulator(l, lexerAtn, lexerDecisionToDFA, antlr.NewPredictionContextCache())

	l.channelNames = lexerChannelNames
	l.modeNames = lexerModeNames
	l.RuleNames = lexerRuleNames
	l.LiteralNames = lexerLiteralNames
	l.SymbolicNames = lexerSymbolicNames
	l.GrammarFileName = "Mlua.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// MluaLexer tokens.
const (
	MluaLexerNilLiteral            = 1
	MluaLexerTrueLiteral           = 2
	MluaLexerFalseLiteral          = 3
	MluaLexerIfLiteral             = 4
	MluaLexerElseLiteral           = 5
	MluaLexerThenLiteral           = 6
	MluaLexerWhileLiteral          = 7
	MluaLexerDoLiteral             = 8
	MluaLexerEndLiteral            = 9
	MluaLexerInputLiteral          = 10
	MluaLexerPrintLiteral          = 11
	MluaLexerLparenLiteral         = 12
	MluaLexerRparenLiteral         = 13
	MluaLexerPlusLiteral           = 14
	MluaLexerMinusLiteral          = 15
	MluaLexerMultiplicationLiteral = 16
	MluaLexerDivisionLiteral       = 17
	MluaLexerDoubleEqualLiteral    = 18
	MluaLexerNotEqualLiteral       = 19
	MluaLexerLessLiteral           = 20
	MluaLexerLessOrEqualLiteral    = 21
	MluaLexerGreaterLiteral        = 22
	MluaLexerGreaterOrEqualLiteral = 23
	MluaLexerEqualLiteral          = 24
	MluaLexerIdenitfier            = 25
	MluaLexerIntegerLiteral        = 26
	MluaLexerWHITESPACE            = 27
)
