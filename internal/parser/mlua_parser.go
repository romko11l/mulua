// Code generated from ./internal/parser/Mlua.g4 by ANTLR 4.7.2. DO NOT EDIT.

package parser // Mlua

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 29, 102,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 3, 2,
	3, 2, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 5, 3, 40, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 3, 54, 10, 3, 12, 3, 14, 3, 57, 11, 3,
	3, 4, 3, 4, 3, 5, 3, 5, 3, 6, 3, 6, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 8,
	3, 8, 3, 8, 3, 8, 3, 9, 3, 9, 3, 9, 3, 9, 3, 9, 3, 9, 3, 9, 5, 9, 81, 10,
	9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 10, 3, 11, 3, 11,
	3, 11, 3, 11, 5, 11, 95, 10, 11, 3, 12, 6, 12, 98, 10, 12, 13, 12, 14,
	12, 99, 3, 12, 2, 3, 4, 13, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 2,
	6, 4, 2, 3, 5, 28, 28, 3, 2, 18, 19, 3, 2, 16, 17, 3, 2, 20, 25, 2, 102,
	2, 24, 3, 2, 2, 2, 4, 39, 3, 2, 2, 2, 6, 58, 3, 2, 2, 2, 8, 60, 3, 2, 2,
	2, 10, 62, 3, 2, 2, 2, 12, 64, 3, 2, 2, 2, 14, 69, 3, 2, 2, 2, 16, 73,
	3, 2, 2, 2, 18, 84, 3, 2, 2, 2, 20, 94, 3, 2, 2, 2, 22, 97, 3, 2, 2, 2,
	24, 25, 5, 22, 12, 2, 25, 26, 7, 2, 2, 3, 26, 3, 3, 2, 2, 2, 27, 28, 8,
	3, 1, 2, 28, 40, 7, 27, 2, 2, 29, 40, 9, 2, 2, 2, 30, 31, 7, 14, 2, 2,
	31, 32, 5, 4, 3, 2, 32, 33, 7, 15, 2, 2, 33, 40, 3, 2, 2, 2, 34, 35, 7,
	12, 2, 2, 35, 36, 7, 14, 2, 2, 36, 40, 7, 15, 2, 2, 37, 38, 7, 17, 2, 2,
	38, 40, 5, 4, 3, 3, 39, 27, 3, 2, 2, 2, 39, 29, 3, 2, 2, 2, 39, 30, 3,
	2, 2, 2, 39, 34, 3, 2, 2, 2, 39, 37, 3, 2, 2, 2, 40, 55, 3, 2, 2, 2, 41,
	42, 12, 8, 2, 2, 42, 43, 5, 6, 4, 2, 43, 44, 5, 4, 3, 9, 44, 54, 3, 2,
	2, 2, 45, 46, 12, 7, 2, 2, 46, 47, 5, 8, 5, 2, 47, 48, 5, 4, 3, 8, 48,
	54, 3, 2, 2, 2, 49, 50, 12, 6, 2, 2, 50, 51, 5, 10, 6, 2, 51, 52, 5, 4,
	3, 7, 52, 54, 3, 2, 2, 2, 53, 41, 3, 2, 2, 2, 53, 45, 3, 2, 2, 2, 53, 49,
	3, 2, 2, 2, 54, 57, 3, 2, 2, 2, 55, 53, 3, 2, 2, 2, 55, 56, 3, 2, 2, 2,
	56, 5, 3, 2, 2, 2, 57, 55, 3, 2, 2, 2, 58, 59, 9, 3, 2, 2, 59, 7, 3, 2,
	2, 2, 60, 61, 9, 4, 2, 2, 61, 9, 3, 2, 2, 2, 62, 63, 9, 5, 2, 2, 63, 11,
	3, 2, 2, 2, 64, 65, 7, 13, 2, 2, 65, 66, 7, 14, 2, 2, 66, 67, 5, 4, 3,
	2, 67, 68, 7, 15, 2, 2, 68, 13, 3, 2, 2, 2, 69, 70, 7, 27, 2, 2, 70, 71,
	7, 26, 2, 2, 71, 72, 5, 4, 3, 2, 72, 15, 3, 2, 2, 2, 73, 74, 7, 6, 2, 2,
	74, 75, 5, 4, 3, 2, 75, 76, 7, 8, 2, 2, 76, 80, 5, 22, 12, 2, 77, 78, 7,
	7, 2, 2, 78, 81, 5, 22, 12, 2, 79, 81, 3, 2, 2, 2, 80, 77, 3, 2, 2, 2,
	80, 79, 3, 2, 2, 2, 81, 82, 3, 2, 2, 2, 82, 83, 7, 11, 2, 2, 83, 17, 3,
	2, 2, 2, 84, 85, 7, 9, 2, 2, 85, 86, 5, 4, 3, 2, 86, 87, 7, 10, 2, 2, 87,
	88, 5, 22, 12, 2, 88, 89, 7, 11, 2, 2, 89, 19, 3, 2, 2, 2, 90, 95, 5, 12,
	7, 2, 91, 95, 5, 14, 8, 2, 92, 95, 5, 16, 9, 2, 93, 95, 5, 18, 10, 2, 94,
	90, 3, 2, 2, 2, 94, 91, 3, 2, 2, 2, 94, 92, 3, 2, 2, 2, 94, 93, 3, 2, 2,
	2, 95, 21, 3, 2, 2, 2, 96, 98, 5, 20, 11, 2, 97, 96, 3, 2, 2, 2, 98, 99,
	3, 2, 2, 2, 99, 97, 3, 2, 2, 2, 99, 100, 3, 2, 2, 2, 100, 23, 3, 2, 2,
	2, 8, 39, 53, 55, 80, 94, 99,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'nil'", "'true'", "'false'", "'if'", "'else'", "'then'", "'while'",
	"'do'", "'end'", "'input'", "'print'", "'('", "')'", "'+'", "'-'", "'*'",
	"'//'", "'=='", "'~='", "'<'", "'<='", "'>'", "'>='", "'='",
}
var symbolicNames = []string{
	"", "NilLiteral", "TrueLiteral", "FalseLiteral", "IfLiteral", "ElseLiteral",
	"ThenLiteral", "WhileLiteral", "DoLiteral", "EndLiteral", "InputLiteral",
	"PrintLiteral", "LparenLiteral", "RparenLiteral", "PlusLiteral", "MinusLiteral",
	"MultiplicationLiteral", "DivisionLiteral", "DoubleEqualLiteral", "NotEqualLiteral",
	"LessLiteral", "LessOrEqualLiteral", "GreaterLiteral", "GreaterOrEqualLiteral",
	"EqualLiteral", "Idenitfier", "IntegerLiteral", "WHITESPACE",
}

var ruleNames = []string{
	"program", "expression", "binaryMulDivOperator", "binaryAddSubOperator",
	"binaryComparisonOperator", "printStatement", "assignmentStatement", "ifStatement",
	"whileStatement", "statement", "blockStatement",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type MluaParser struct {
	*antlr.BaseParser
}

func NewMluaParser(input antlr.TokenStream) *MluaParser {
	this := new(MluaParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Mlua.g4"

	return this
}

// MluaParser tokens.
const (
	MluaParserEOF                   = antlr.TokenEOF
	MluaParserNilLiteral            = 1
	MluaParserTrueLiteral           = 2
	MluaParserFalseLiteral          = 3
	MluaParserIfLiteral             = 4
	MluaParserElseLiteral           = 5
	MluaParserThenLiteral           = 6
	MluaParserWhileLiteral          = 7
	MluaParserDoLiteral             = 8
	MluaParserEndLiteral            = 9
	MluaParserInputLiteral          = 10
	MluaParserPrintLiteral          = 11
	MluaParserLparenLiteral         = 12
	MluaParserRparenLiteral         = 13
	MluaParserPlusLiteral           = 14
	MluaParserMinusLiteral          = 15
	MluaParserMultiplicationLiteral = 16
	MluaParserDivisionLiteral       = 17
	MluaParserDoubleEqualLiteral    = 18
	MluaParserNotEqualLiteral       = 19
	MluaParserLessLiteral           = 20
	MluaParserLessOrEqualLiteral    = 21
	MluaParserGreaterLiteral        = 22
	MluaParserGreaterOrEqualLiteral = 23
	MluaParserEqualLiteral          = 24
	MluaParserIdenitfier            = 25
	MluaParserIntegerLiteral        = 26
	MluaParserWHITESPACE            = 27
)

// MluaParser rules.
const (
	MluaParserRULE_program                  = 0
	MluaParserRULE_expression               = 1
	MluaParserRULE_binaryMulDivOperator     = 2
	MluaParserRULE_binaryAddSubOperator     = 3
	MluaParserRULE_binaryComparisonOperator = 4
	MluaParserRULE_printStatement           = 5
	MluaParserRULE_assignmentStatement      = 6
	MluaParserRULE_ifStatement              = 7
	MluaParserRULE_whileStatement           = 8
	MluaParserRULE_statement                = 9
	MluaParserRULE_blockStatement           = 10
)

// IProgramContext is an interface to support dynamic dispatch.
type IProgramContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetBlock returns the block rule contexts.
	GetBlock() IBlockStatementContext

	// SetBlock sets the block rule contexts.
	SetBlock(IBlockStatementContext)

	// IsProgramContext differentiates from other interfaces.
	IsProgramContext()
}

type ProgramContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	block  IBlockStatementContext
}

func NewEmptyProgramContext() *ProgramContext {
	var p = new(ProgramContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_program
	return p
}

func (*ProgramContext) IsProgramContext() {}

func NewProgramContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProgramContext {
	var p = new(ProgramContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_program

	return p
}

func (s *ProgramContext) GetParser() antlr.Parser { return s.parser }

func (s *ProgramContext) GetBlock() IBlockStatementContext { return s.block }

func (s *ProgramContext) SetBlock(v IBlockStatementContext) { s.block = v }

func (s *ProgramContext) EOF() antlr.TerminalNode {
	return s.GetToken(MluaParserEOF, 0)
}

func (s *ProgramContext) BlockStatement() IBlockStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBlockStatementContext)
}

func (s *ProgramContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProgramContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProgramContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitProgram(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) Program() (localctx IProgramContext) {
	localctx = NewProgramContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, MluaParserRULE_program)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(22)

		var _x = p.BlockStatement()

		localctx.(*ProgramContext).block = _x
	}
	{
		p.SetState(23)
		p.Match(MluaParserEOF)
	}

	return localctx
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_expression
	return p
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) CopyFrom(ctx *ExpressionContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type IdenitfierExpressionContext struct {
	*ExpressionContext
}

func NewIdenitfierExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IdenitfierExpressionContext {
	var p = new(IdenitfierExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *IdenitfierExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdenitfierExpressionContext) Idenitfier() antlr.TerminalNode {
	return s.GetToken(MluaParserIdenitfier, 0)
}

func (s *IdenitfierExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitIdenitfierExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type ParenthesizedExpressionContext struct {
	*ExpressionContext
	expr IExpressionContext
}

func NewParenthesizedExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ParenthesizedExpressionContext {
	var p = new(ParenthesizedExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ParenthesizedExpressionContext) GetExpr() IExpressionContext { return s.expr }

func (s *ParenthesizedExpressionContext) SetExpr(v IExpressionContext) { s.expr = v }

func (s *ParenthesizedExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParenthesizedExpressionContext) LparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserLparenLiteral, 0)
}

func (s *ParenthesizedExpressionContext) RparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserRparenLiteral, 0)
}

func (s *ParenthesizedExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ParenthesizedExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitParenthesizedExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type BinaryComparisonExpressionContext struct {
	*ExpressionContext
	left     IExpressionContext
	operator IBinaryComparisonOperatorContext
	right    IExpressionContext
}

func NewBinaryComparisonExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BinaryComparisonExpressionContext {
	var p = new(BinaryComparisonExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BinaryComparisonExpressionContext) GetLeft() IExpressionContext { return s.left }

func (s *BinaryComparisonExpressionContext) GetOperator() IBinaryComparisonOperatorContext {
	return s.operator
}

func (s *BinaryComparisonExpressionContext) GetRight() IExpressionContext { return s.right }

func (s *BinaryComparisonExpressionContext) SetLeft(v IExpressionContext) { s.left = v }

func (s *BinaryComparisonExpressionContext) SetOperator(v IBinaryComparisonOperatorContext) {
	s.operator = v
}

func (s *BinaryComparisonExpressionContext) SetRight(v IExpressionContext) { s.right = v }

func (s *BinaryComparisonExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryComparisonExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *BinaryComparisonExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *BinaryComparisonExpressionContext) BinaryComparisonOperator() IBinaryComparisonOperatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBinaryComparisonOperatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBinaryComparisonOperatorContext)
}

func (s *BinaryComparisonExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryComparisonExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type LiteralExpressionContext struct {
	*ExpressionContext
	lit antlr.Token
}

func NewLiteralExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LiteralExpressionContext {
	var p = new(LiteralExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *LiteralExpressionContext) GetLit() antlr.Token { return s.lit }

func (s *LiteralExpressionContext) SetLit(v antlr.Token) { s.lit = v }

func (s *LiteralExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LiteralExpressionContext) NilLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserNilLiteral, 0)
}

func (s *LiteralExpressionContext) TrueLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserTrueLiteral, 0)
}

func (s *LiteralExpressionContext) FalseLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserFalseLiteral, 0)
}

func (s *LiteralExpressionContext) IntegerLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserIntegerLiteral, 0)
}

func (s *LiteralExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitLiteralExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type UnaryExpressionContext struct {
	*ExpressionContext
	expr IExpressionContext
}

func NewUnaryExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *UnaryExpressionContext {
	var p = new(UnaryExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *UnaryExpressionContext) GetExpr() IExpressionContext { return s.expr }

func (s *UnaryExpressionContext) SetExpr(v IExpressionContext) { s.expr = v }

func (s *UnaryExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UnaryExpressionContext) MinusLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserMinusLiteral, 0)
}

func (s *UnaryExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *UnaryExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitUnaryExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type InputExpressionContext struct {
	*ExpressionContext
}

func NewInputExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *InputExpressionContext {
	var p = new(InputExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *InputExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *InputExpressionContext) InputLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserInputLiteral, 0)
}

func (s *InputExpressionContext) LparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserLparenLiteral, 0)
}

func (s *InputExpressionContext) RparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserRparenLiteral, 0)
}

func (s *InputExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitInputExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type BinaryMulDivExpressionContext struct {
	*ExpressionContext
	left     IExpressionContext
	operator IBinaryMulDivOperatorContext
	right    IExpressionContext
}

func NewBinaryMulDivExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BinaryMulDivExpressionContext {
	var p = new(BinaryMulDivExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BinaryMulDivExpressionContext) GetLeft() IExpressionContext { return s.left }

func (s *BinaryMulDivExpressionContext) GetOperator() IBinaryMulDivOperatorContext { return s.operator }

func (s *BinaryMulDivExpressionContext) GetRight() IExpressionContext { return s.right }

func (s *BinaryMulDivExpressionContext) SetLeft(v IExpressionContext) { s.left = v }

func (s *BinaryMulDivExpressionContext) SetOperator(v IBinaryMulDivOperatorContext) { s.operator = v }

func (s *BinaryMulDivExpressionContext) SetRight(v IExpressionContext) { s.right = v }

func (s *BinaryMulDivExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryMulDivExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *BinaryMulDivExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *BinaryMulDivExpressionContext) BinaryMulDivOperator() IBinaryMulDivOperatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBinaryMulDivOperatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBinaryMulDivOperatorContext)
}

func (s *BinaryMulDivExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryMulDivExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

type BinaryAddSubExpressionContext struct {
	*ExpressionContext
	left     IExpressionContext
	operator IBinaryAddSubOperatorContext
	right    IExpressionContext
}

func NewBinaryAddSubExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BinaryAddSubExpressionContext {
	var p = new(BinaryAddSubExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BinaryAddSubExpressionContext) GetLeft() IExpressionContext { return s.left }

func (s *BinaryAddSubExpressionContext) GetOperator() IBinaryAddSubOperatorContext { return s.operator }

func (s *BinaryAddSubExpressionContext) GetRight() IExpressionContext { return s.right }

func (s *BinaryAddSubExpressionContext) SetLeft(v IExpressionContext) { s.left = v }

func (s *BinaryAddSubExpressionContext) SetOperator(v IBinaryAddSubOperatorContext) { s.operator = v }

func (s *BinaryAddSubExpressionContext) SetRight(v IExpressionContext) { s.right = v }

func (s *BinaryAddSubExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryAddSubExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *BinaryAddSubExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *BinaryAddSubExpressionContext) BinaryAddSubOperator() IBinaryAddSubOperatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBinaryAddSubOperatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBinaryAddSubOperatorContext)
}

func (s *BinaryAddSubExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryAddSubExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *MluaParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 2
	p.EnterRecursionRule(localctx, 2, MluaParserRULE_expression, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(37)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case MluaParserIdenitfier:
		localctx = NewIdenitfierExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(26)
			p.Match(MluaParserIdenitfier)
		}

	case MluaParserNilLiteral, MluaParserTrueLiteral, MluaParserFalseLiteral, MluaParserIntegerLiteral:
		localctx = NewLiteralExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(27)

			var _lt = p.GetTokenStream().LT(1)

			localctx.(*LiteralExpressionContext).lit = _lt

			_la = p.GetTokenStream().LA(1)

			if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<MluaParserNilLiteral)|(1<<MluaParserTrueLiteral)|(1<<MluaParserFalseLiteral)|(1<<MluaParserIntegerLiteral))) != 0) {
				var _ri = p.GetErrorHandler().RecoverInline(p)

				localctx.(*LiteralExpressionContext).lit = _ri
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}

	case MluaParserLparenLiteral:
		localctx = NewParenthesizedExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(28)
			p.Match(MluaParserLparenLiteral)
		}
		{
			p.SetState(29)

			var _x = p.expression(0)

			localctx.(*ParenthesizedExpressionContext).expr = _x
		}
		{
			p.SetState(30)
			p.Match(MluaParserRparenLiteral)
		}

	case MluaParserInputLiteral:
		localctx = NewInputExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(32)
			p.Match(MluaParserInputLiteral)
		}
		{
			p.SetState(33)
			p.Match(MluaParserLparenLiteral)
		}
		{
			p.SetState(34)
			p.Match(MluaParserRparenLiteral)
		}

	case MluaParserMinusLiteral:
		localctx = NewUnaryExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(35)
			p.Match(MluaParserMinusLiteral)
		}
		{
			p.SetState(36)

			var _x = p.expression(1)

			localctx.(*UnaryExpressionContext).expr = _x
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(53)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 2, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(51)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 1, p.GetParserRuleContext()) {
			case 1:
				localctx = NewBinaryMulDivExpressionContext(p, NewExpressionContext(p, _parentctx, _parentState))
				localctx.(*BinaryMulDivExpressionContext).left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, MluaParserRULE_expression)
				p.SetState(39)

				if !(p.Precpred(p.GetParserRuleContext(), 6)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 6)", ""))
				}
				{
					p.SetState(40)

					var _x = p.BinaryMulDivOperator()

					localctx.(*BinaryMulDivExpressionContext).operator = _x
				}
				{
					p.SetState(41)

					var _x = p.expression(7)

					localctx.(*BinaryMulDivExpressionContext).right = _x
				}

			case 2:
				localctx = NewBinaryAddSubExpressionContext(p, NewExpressionContext(p, _parentctx, _parentState))
				localctx.(*BinaryAddSubExpressionContext).left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, MluaParserRULE_expression)
				p.SetState(43)

				if !(p.Precpred(p.GetParserRuleContext(), 5)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 5)", ""))
				}
				{
					p.SetState(44)

					var _x = p.BinaryAddSubOperator()

					localctx.(*BinaryAddSubExpressionContext).operator = _x
				}
				{
					p.SetState(45)

					var _x = p.expression(6)

					localctx.(*BinaryAddSubExpressionContext).right = _x
				}

			case 3:
				localctx = NewBinaryComparisonExpressionContext(p, NewExpressionContext(p, _parentctx, _parentState))
				localctx.(*BinaryComparisonExpressionContext).left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, MluaParserRULE_expression)
				p.SetState(47)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
				}
				{
					p.SetState(48)

					var _x = p.BinaryComparisonOperator()

					localctx.(*BinaryComparisonExpressionContext).operator = _x
				}
				{
					p.SetState(49)

					var _x = p.expression(5)

					localctx.(*BinaryComparisonExpressionContext).right = _x
				}

			}

		}
		p.SetState(55)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 2, p.GetParserRuleContext())
	}

	return localctx
}

// IBinaryMulDivOperatorContext is an interface to support dynamic dispatch.
type IBinaryMulDivOperatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBinaryMulDivOperatorContext differentiates from other interfaces.
	IsBinaryMulDivOperatorContext()
}

type BinaryMulDivOperatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBinaryMulDivOperatorContext() *BinaryMulDivOperatorContext {
	var p = new(BinaryMulDivOperatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_binaryMulDivOperator
	return p
}

func (*BinaryMulDivOperatorContext) IsBinaryMulDivOperatorContext() {}

func NewBinaryMulDivOperatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BinaryMulDivOperatorContext {
	var p = new(BinaryMulDivOperatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_binaryMulDivOperator

	return p
}

func (s *BinaryMulDivOperatorContext) GetParser() antlr.Parser { return s.parser }

func (s *BinaryMulDivOperatorContext) MultiplicationLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserMultiplicationLiteral, 0)
}

func (s *BinaryMulDivOperatorContext) DivisionLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserDivisionLiteral, 0)
}

func (s *BinaryMulDivOperatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryMulDivOperatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BinaryMulDivOperatorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryMulDivOperator(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) BinaryMulDivOperator() (localctx IBinaryMulDivOperatorContext) {
	localctx = NewBinaryMulDivOperatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, MluaParserRULE_binaryMulDivOperator)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(56)
		_la = p.GetTokenStream().LA(1)

		if !(_la == MluaParserMultiplicationLiteral || _la == MluaParserDivisionLiteral) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IBinaryAddSubOperatorContext is an interface to support dynamic dispatch.
type IBinaryAddSubOperatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBinaryAddSubOperatorContext differentiates from other interfaces.
	IsBinaryAddSubOperatorContext()
}

type BinaryAddSubOperatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBinaryAddSubOperatorContext() *BinaryAddSubOperatorContext {
	var p = new(BinaryAddSubOperatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_binaryAddSubOperator
	return p
}

func (*BinaryAddSubOperatorContext) IsBinaryAddSubOperatorContext() {}

func NewBinaryAddSubOperatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BinaryAddSubOperatorContext {
	var p = new(BinaryAddSubOperatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_binaryAddSubOperator

	return p
}

func (s *BinaryAddSubOperatorContext) GetParser() antlr.Parser { return s.parser }

func (s *BinaryAddSubOperatorContext) PlusLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserPlusLiteral, 0)
}

func (s *BinaryAddSubOperatorContext) MinusLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserMinusLiteral, 0)
}

func (s *BinaryAddSubOperatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryAddSubOperatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BinaryAddSubOperatorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryAddSubOperator(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) BinaryAddSubOperator() (localctx IBinaryAddSubOperatorContext) {
	localctx = NewBinaryAddSubOperatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, MluaParserRULE_binaryAddSubOperator)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(58)
		_la = p.GetTokenStream().LA(1)

		if !(_la == MluaParserPlusLiteral || _la == MluaParserMinusLiteral) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IBinaryComparisonOperatorContext is an interface to support dynamic dispatch.
type IBinaryComparisonOperatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBinaryComparisonOperatorContext differentiates from other interfaces.
	IsBinaryComparisonOperatorContext()
}

type BinaryComparisonOperatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBinaryComparisonOperatorContext() *BinaryComparisonOperatorContext {
	var p = new(BinaryComparisonOperatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_binaryComparisonOperator
	return p
}

func (*BinaryComparisonOperatorContext) IsBinaryComparisonOperatorContext() {}

func NewBinaryComparisonOperatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BinaryComparisonOperatorContext {
	var p = new(BinaryComparisonOperatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_binaryComparisonOperator

	return p
}

func (s *BinaryComparisonOperatorContext) GetParser() antlr.Parser { return s.parser }

func (s *BinaryComparisonOperatorContext) DoubleEqualLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserDoubleEqualLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) NotEqualLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserNotEqualLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) LessLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserLessLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) LessOrEqualLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserLessOrEqualLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) GreaterLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserGreaterLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) GreaterOrEqualLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserGreaterOrEqualLiteral, 0)
}

func (s *BinaryComparisonOperatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryComparisonOperatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BinaryComparisonOperatorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBinaryComparisonOperator(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) BinaryComparisonOperator() (localctx IBinaryComparisonOperatorContext) {
	localctx = NewBinaryComparisonOperatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, MluaParserRULE_binaryComparisonOperator)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<MluaParserDoubleEqualLiteral)|(1<<MluaParserNotEqualLiteral)|(1<<MluaParserLessLiteral)|(1<<MluaParserLessOrEqualLiteral)|(1<<MluaParserGreaterLiteral)|(1<<MluaParserGreaterOrEqualLiteral))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IPrintStatementContext is an interface to support dynamic dispatch.
type IPrintStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetExpr returns the expr rule contexts.
	GetExpr() IExpressionContext

	// SetExpr sets the expr rule contexts.
	SetExpr(IExpressionContext)

	// IsPrintStatementContext differentiates from other interfaces.
	IsPrintStatementContext()
}

type PrintStatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	expr   IExpressionContext
}

func NewEmptyPrintStatementContext() *PrintStatementContext {
	var p = new(PrintStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_printStatement
	return p
}

func (*PrintStatementContext) IsPrintStatementContext() {}

func NewPrintStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PrintStatementContext {
	var p = new(PrintStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_printStatement

	return p
}

func (s *PrintStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *PrintStatementContext) GetExpr() IExpressionContext { return s.expr }

func (s *PrintStatementContext) SetExpr(v IExpressionContext) { s.expr = v }

func (s *PrintStatementContext) PrintLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserPrintLiteral, 0)
}

func (s *PrintStatementContext) LparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserLparenLiteral, 0)
}

func (s *PrintStatementContext) RparenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserRparenLiteral, 0)
}

func (s *PrintStatementContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *PrintStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PrintStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PrintStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitPrintStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) PrintStatement() (localctx IPrintStatementContext) {
	localctx = NewPrintStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, MluaParserRULE_printStatement)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(62)
		p.Match(MluaParserPrintLiteral)
	}
	{
		p.SetState(63)
		p.Match(MluaParserLparenLiteral)
	}
	{
		p.SetState(64)

		var _x = p.expression(0)

		localctx.(*PrintStatementContext).expr = _x
	}
	{
		p.SetState(65)
		p.Match(MluaParserRparenLiteral)
	}

	return localctx
}

// IAssignmentStatementContext is an interface to support dynamic dispatch.
type IAssignmentStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetLeft returns the left token.
	GetLeft() antlr.Token

	// SetLeft sets the left token.
	SetLeft(antlr.Token)

	// GetRight returns the right rule contexts.
	GetRight() IExpressionContext

	// SetRight sets the right rule contexts.
	SetRight(IExpressionContext)

	// IsAssignmentStatementContext differentiates from other interfaces.
	IsAssignmentStatementContext()
}

type AssignmentStatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	left   antlr.Token
	right  IExpressionContext
}

func NewEmptyAssignmentStatementContext() *AssignmentStatementContext {
	var p = new(AssignmentStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_assignmentStatement
	return p
}

func (*AssignmentStatementContext) IsAssignmentStatementContext() {}

func NewAssignmentStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AssignmentStatementContext {
	var p = new(AssignmentStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_assignmentStatement

	return p
}

func (s *AssignmentStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *AssignmentStatementContext) GetLeft() antlr.Token { return s.left }

func (s *AssignmentStatementContext) SetLeft(v antlr.Token) { s.left = v }

func (s *AssignmentStatementContext) GetRight() IExpressionContext { return s.right }

func (s *AssignmentStatementContext) SetRight(v IExpressionContext) { s.right = v }

func (s *AssignmentStatementContext) EqualLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserEqualLiteral, 0)
}

func (s *AssignmentStatementContext) Idenitfier() antlr.TerminalNode {
	return s.GetToken(MluaParserIdenitfier, 0)
}

func (s *AssignmentStatementContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *AssignmentStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AssignmentStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AssignmentStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitAssignmentStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) AssignmentStatement() (localctx IAssignmentStatementContext) {
	localctx = NewAssignmentStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, MluaParserRULE_assignmentStatement)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(67)

		var _m = p.Match(MluaParserIdenitfier)

		localctx.(*AssignmentStatementContext).left = _m
	}
	{
		p.SetState(68)
		p.Match(MluaParserEqualLiteral)
	}
	{
		p.SetState(69)

		var _x = p.expression(0)

		localctx.(*AssignmentStatementContext).right = _x
	}

	return localctx
}

// IIfStatementContext is an interface to support dynamic dispatch.
type IIfStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetCondition returns the condition rule contexts.
	GetCondition() IExpressionContext

	// GetIfbody returns the ifbody rule contexts.
	GetIfbody() IBlockStatementContext

	// GetElsebody returns the elsebody rule contexts.
	GetElsebody() IBlockStatementContext

	// SetCondition sets the condition rule contexts.
	SetCondition(IExpressionContext)

	// SetIfbody sets the ifbody rule contexts.
	SetIfbody(IBlockStatementContext)

	// SetElsebody sets the elsebody rule contexts.
	SetElsebody(IBlockStatementContext)

	// IsIfStatementContext differentiates from other interfaces.
	IsIfStatementContext()
}

type IfStatementContext struct {
	*antlr.BaseParserRuleContext
	parser    antlr.Parser
	condition IExpressionContext
	ifbody    IBlockStatementContext
	elsebody  IBlockStatementContext
}

func NewEmptyIfStatementContext() *IfStatementContext {
	var p = new(IfStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_ifStatement
	return p
}

func (*IfStatementContext) IsIfStatementContext() {}

func NewIfStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfStatementContext {
	var p = new(IfStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_ifStatement

	return p
}

func (s *IfStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *IfStatementContext) GetCondition() IExpressionContext { return s.condition }

func (s *IfStatementContext) GetIfbody() IBlockStatementContext { return s.ifbody }

func (s *IfStatementContext) GetElsebody() IBlockStatementContext { return s.elsebody }

func (s *IfStatementContext) SetCondition(v IExpressionContext) { s.condition = v }

func (s *IfStatementContext) SetIfbody(v IBlockStatementContext) { s.ifbody = v }

func (s *IfStatementContext) SetElsebody(v IBlockStatementContext) { s.elsebody = v }

func (s *IfStatementContext) IfLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserIfLiteral, 0)
}

func (s *IfStatementContext) ThenLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserThenLiteral, 0)
}

func (s *IfStatementContext) EndLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserEndLiteral, 0)
}

func (s *IfStatementContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IfStatementContext) AllBlockStatement() []IBlockStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IBlockStatementContext)(nil)).Elem())
	var tst = make([]IBlockStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IBlockStatementContext)
		}
	}

	return tst
}

func (s *IfStatementContext) BlockStatement(i int) IBlockStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IBlockStatementContext)
}

func (s *IfStatementContext) ElseLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserElseLiteral, 0)
}

func (s *IfStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitIfStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) IfStatement() (localctx IIfStatementContext) {
	localctx = NewIfStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, MluaParserRULE_ifStatement)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(71)
		p.Match(MluaParserIfLiteral)
	}
	{
		p.SetState(72)

		var _x = p.expression(0)

		localctx.(*IfStatementContext).condition = _x
	}
	{
		p.SetState(73)
		p.Match(MluaParserThenLiteral)
	}
	{
		p.SetState(74)

		var _x = p.BlockStatement()

		localctx.(*IfStatementContext).ifbody = _x
	}
	p.SetState(78)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case MluaParserElseLiteral:
		{
			p.SetState(75)
			p.Match(MluaParserElseLiteral)
		}
		{
			p.SetState(76)

			var _x = p.BlockStatement()

			localctx.(*IfStatementContext).elsebody = _x
		}

	case MluaParserEndLiteral:

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	{
		p.SetState(80)
		p.Match(MluaParserEndLiteral)
	}

	return localctx
}

// IWhileStatementContext is an interface to support dynamic dispatch.
type IWhileStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetCondition returns the condition rule contexts.
	GetCondition() IExpressionContext

	// GetBody returns the body rule contexts.
	GetBody() IBlockStatementContext

	// SetCondition sets the condition rule contexts.
	SetCondition(IExpressionContext)

	// SetBody sets the body rule contexts.
	SetBody(IBlockStatementContext)

	// IsWhileStatementContext differentiates from other interfaces.
	IsWhileStatementContext()
}

type WhileStatementContext struct {
	*antlr.BaseParserRuleContext
	parser    antlr.Parser
	condition IExpressionContext
	body      IBlockStatementContext
}

func NewEmptyWhileStatementContext() *WhileStatementContext {
	var p = new(WhileStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_whileStatement
	return p
}

func (*WhileStatementContext) IsWhileStatementContext() {}

func NewWhileStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *WhileStatementContext {
	var p = new(WhileStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_whileStatement

	return p
}

func (s *WhileStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *WhileStatementContext) GetCondition() IExpressionContext { return s.condition }

func (s *WhileStatementContext) GetBody() IBlockStatementContext { return s.body }

func (s *WhileStatementContext) SetCondition(v IExpressionContext) { s.condition = v }

func (s *WhileStatementContext) SetBody(v IBlockStatementContext) { s.body = v }

func (s *WhileStatementContext) WhileLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserWhileLiteral, 0)
}

func (s *WhileStatementContext) DoLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserDoLiteral, 0)
}

func (s *WhileStatementContext) EndLiteral() antlr.TerminalNode {
	return s.GetToken(MluaParserEndLiteral, 0)
}

func (s *WhileStatementContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *WhileStatementContext) BlockStatement() IBlockStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBlockStatementContext)
}

func (s *WhileStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *WhileStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *WhileStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitWhileStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) WhileStatement() (localctx IWhileStatementContext) {
	localctx = NewWhileStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, MluaParserRULE_whileStatement)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(82)
		p.Match(MluaParserWhileLiteral)
	}
	{
		p.SetState(83)

		var _x = p.expression(0)

		localctx.(*WhileStatementContext).condition = _x
	}
	{
		p.SetState(84)
		p.Match(MluaParserDoLiteral)
	}
	{
		p.SetState(85)

		var _x = p.BlockStatement()

		localctx.(*WhileStatementContext).body = _x
	}
	{
		p.SetState(86)
		p.Match(MluaParserEndLiteral)
	}

	return localctx
}

// IStatementContext is an interface to support dynamic dispatch.
type IStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStatementContext differentiates from other interfaces.
	IsStatementContext()
}

type StatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStatementContext() *StatementContext {
	var p = new(StatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_statement
	return p
}

func (*StatementContext) IsStatementContext() {}

func NewStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StatementContext {
	var p = new(StatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_statement

	return p
}

func (s *StatementContext) GetParser() antlr.Parser { return s.parser }

func (s *StatementContext) PrintStatement() IPrintStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPrintStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPrintStatementContext)
}

func (s *StatementContext) AssignmentStatement() IAssignmentStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAssignmentStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAssignmentStatementContext)
}

func (s *StatementContext) IfStatement() IIfStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfStatementContext)
}

func (s *StatementContext) WhileStatement() IWhileStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IWhileStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IWhileStatementContext)
}

func (s *StatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) Statement() (localctx IStatementContext) {
	localctx = NewStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, MluaParserRULE_statement)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(92)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case MluaParserPrintLiteral:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(88)
			p.PrintStatement()
		}

	case MluaParserIdenitfier:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(89)
			p.AssignmentStatement()
		}

	case MluaParserIfLiteral:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(90)
			p.IfStatement()
		}

	case MluaParserWhileLiteral:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(91)
			p.WhileStatement()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IBlockStatementContext is an interface to support dynamic dispatch.
type IBlockStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBlockStatementContext differentiates from other interfaces.
	IsBlockStatementContext()
}

type BlockStatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBlockStatementContext() *BlockStatementContext {
	var p = new(BlockStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = MluaParserRULE_blockStatement
	return p
}

func (*BlockStatementContext) IsBlockStatementContext() {}

func NewBlockStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BlockStatementContext {
	var p = new(BlockStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = MluaParserRULE_blockStatement

	return p
}

func (s *BlockStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *BlockStatementContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *BlockStatementContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *BlockStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BlockStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BlockStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case MluaVisitor:
		return t.VisitBlockStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *MluaParser) BlockStatement() (localctx IBlockStatementContext) {
	localctx = NewBlockStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, MluaParserRULE_blockStatement)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(95)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<MluaParserIfLiteral)|(1<<MluaParserWhileLiteral)|(1<<MluaParserPrintLiteral)|(1<<MluaParserIdenitfier))) != 0) {
		{
			p.SetState(94)
			p.Statement()
		}

		p.SetState(97)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

func (p *MluaParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 1:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *MluaParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 6)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 5)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 4)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
