grammar Mlua;

fragment DIGIT : [0-9] ;
fragment LETTER : [a-zA-Z] ;


// Tokens
NilLiteral              : 'nil' ;
TrueLiteral             : 'true' ;
FalseLiteral            : 'false' ;
IfLiteral               : 'if' ;
ElseLiteral             : 'else' ;
ThenLiteral             : 'then' ;
WhileLiteral            : 'while' ;
DoLiteral               : 'do' ;
EndLiteral              : 'end' ;
InputLiteral            : 'input' ;
PrintLiteral            : 'print' ;
LparenLiteral           : '(' ;
RparenLiteral           : ')' ;
PlusLiteral             : '+' ;
MinusLiteral            : '-' ;
MultiplicationLiteral   : '*' ;
DivisionLiteral         : '//' ;
DoubleEqualLiteral      : '==' ;
NotEqualLiteral         : '~=' ;
LessLiteral             : '<' ;
LessOrEqualLiteral      : '<=' ;
GreaterLiteral          : '>' ;
GreaterOrEqualLiteral   : '>=' ;
EqualLiteral            : '=' ; 

Idenitfier              : LETTER+(DIGIT+|) ;
IntegerLiteral          : DIGIT+;

WHITESPACE              : [ \r\n\t]+ -> skip;


// Rules
program : block = blockStatement EOF
        ;

expression 
      : Idenitfier                                                                  # IdenitfierExpression
      | lit = (NilLiteral | TrueLiteral | FalseLiteral | IntegerLiteral)            # LiteralExpression
      | left = expression operator = binaryMulDivOperator right = expression        # BinaryMulDivExpression
      | left = expression operator = binaryAddSubOperator right = expression        # BinaryAddSubExpression
      | left = expression operator = binaryComparisonOperator right = expression    # binaryComparisonExpression
      | LparenLiteral expr = expression RparenLiteral                               # ParenthesizedExpression
      | InputLiteral LparenLiteral RparenLiteral                                    # InputExpression
      | MinusLiteral expr = expression                                              # UnaryExpression
      ;

binaryMulDivOperator : MultiplicationLiteral | DivisionLiteral ;
binaryAddSubOperator : PlusLiteral | MinusLiteral ;
binaryComparisonOperator : DoubleEqualLiteral | NotEqualLiteral | LessLiteral | LessOrEqualLiteral | GreaterLiteral | GreaterOrEqualLiteral ;

printStatement : PrintLiteral LparenLiteral expr = expression RparenLiteral ;

assignmentStatement : left = Idenitfier EqualLiteral right = expression ;   

ifStatement : IfLiteral condition = expression ThenLiteral ifbody = blockStatement (ElseLiteral elsebody = blockStatement | ) EndLiteral ;

whileStatement : WhileLiteral condition = expression DoLiteral body = blockStatement EndLiteral ;

statement : printStatement
          | assignmentStatement
          | ifStatement
          | whileStatement
          ;

blockStatement : statement+
               ;          