// Code generated from ./internal/parser/Mlua.g4 by ANTLR 4.7.2. DO NOT EDIT.

package parser // Mlua

import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by MluaParser.
type MluaVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by MluaParser#program.
	VisitProgram(ctx *ProgramContext) interface{}

	// Visit a parse tree produced by MluaParser#IdenitfierExpression.
	VisitIdenitfierExpression(ctx *IdenitfierExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#ParenthesizedExpression.
	VisitParenthesizedExpression(ctx *ParenthesizedExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#binaryComparisonExpression.
	VisitBinaryComparisonExpression(ctx *BinaryComparisonExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#LiteralExpression.
	VisitLiteralExpression(ctx *LiteralExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#UnaryExpression.
	VisitUnaryExpression(ctx *UnaryExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#InputExpression.
	VisitInputExpression(ctx *InputExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#BinaryMulDivExpression.
	VisitBinaryMulDivExpression(ctx *BinaryMulDivExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#BinaryAddSubExpression.
	VisitBinaryAddSubExpression(ctx *BinaryAddSubExpressionContext) interface{}

	// Visit a parse tree produced by MluaParser#binaryMulDivOperator.
	VisitBinaryMulDivOperator(ctx *BinaryMulDivOperatorContext) interface{}

	// Visit a parse tree produced by MluaParser#binaryAddSubOperator.
	VisitBinaryAddSubOperator(ctx *BinaryAddSubOperatorContext) interface{}

	// Visit a parse tree produced by MluaParser#binaryComparisonOperator.
	VisitBinaryComparisonOperator(ctx *BinaryComparisonOperatorContext) interface{}

	// Visit a parse tree produced by MluaParser#printStatement.
	VisitPrintStatement(ctx *PrintStatementContext) interface{}

	// Visit a parse tree produced by MluaParser#assignmentStatement.
	VisitAssignmentStatement(ctx *AssignmentStatementContext) interface{}

	// Visit a parse tree produced by MluaParser#ifStatement.
	VisitIfStatement(ctx *IfStatementContext) interface{}

	// Visit a parse tree produced by MluaParser#whileStatement.
	VisitWhileStatement(ctx *WhileStatementContext) interface{}

	// Visit a parse tree produced by MluaParser#statement.
	VisitStatement(ctx *StatementContext) interface{}

	// Visit a parse tree produced by MluaParser#blockStatement.
	VisitBlockStatement(ctx *BlockStatementContext) interface{}
}
