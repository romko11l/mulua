package runtime

import (
	"gitlab.com/romko11l/mulua/internal/parser"
)

func (v *MluaVisitor) VisitBlockStatement(ctx *parser.BlockStatementContext) interface{} {
	statementsList := ctx.GetChildren()
	for _, child := range statementsList {
		v.VisitStatement(child.(*parser.StatementContext))
	}
	return v.VisitChildren(ctx)
}
