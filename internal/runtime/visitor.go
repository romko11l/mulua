package runtime

import (
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/romko11l/mulua/internal/parser"
)

type MluaVisitor struct {
	antlr.ParseTreeVisitor
	env map[string]string
}

func NewMluaVisitor() *MluaVisitor {
	return &MluaVisitor{
		ParseTreeVisitor: &parser.BaseMluaVisitor{},
		env:              make(map[string]string),
	}
}
