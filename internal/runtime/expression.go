package runtime

import (
	"fmt"
	"log"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/romko11l/mulua/internal/parser"
)

func (v *MluaVisitor) VisitExpressionNode(expr antlr.Tree) string {
	var res string
	switch expr := expr.(type) {
	case *parser.IdenitfierExpressionContext:
		res = v.VisitIdenitfierExpression(expr)
	case *parser.ParenthesizedExpressionContext:
		res = v.VisitParenthesizedExpression(expr)
	case *parser.BinaryMulDivExpressionContext:
		res = v.VisitBinaryMulDivExpression(expr)
	case *parser.BinaryAddSubExpressionContext:
		res = v.VisitBinaryAddSubExpression(expr)
	case *parser.BinaryComparisonExpressionContext:
		res = v.VisitBinaryComparisonExpression(expr)
	case *parser.LiteralExpressionContext:
		res = v.VisitLiteralExpression(expr)
	case *parser.UnaryExpressionContext:
		res = v.VisitUnaryExpression(expr)
	case *parser.InputExpressionContext:
		res = v.VisitInputExpression(expr)
	}
	return res
}

func (v *MluaVisitor) VisitIdenitfierExpression(ctx *parser.IdenitfierExpressionContext) string {
	key := ctx.GetText()
	if val, ok := v.env[key]; ok {
		return val
	}
	return "nil"
}

func (v *MluaVisitor) VisitParenthesizedExpression(ctx *parser.ParenthesizedExpressionContext) string {
	return v.VisitExpressionNode(ctx.GetExpr())
}

func (v *MluaVisitor) VisitBinaryMulDivExpression(ctx *parser.BinaryMulDivExpressionContext) string {
	left := v.VisitExpressionNode(ctx.GetLeft())
	leftInt, err := strconv.Atoi(left)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	right := v.VisitExpressionNode(ctx.GetRight())
	rightInt, err := strconv.Atoi(right)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	operator := ctx.GetOperator().GetText()
	var res string
	switch operator {
	case "*":
		res = strconv.Itoa(leftInt * rightInt)
	case "//":
		if rightInt == 0 {
			res = "nil"
		} else {
			res = strconv.Itoa(leftInt / rightInt)
		}
	}
	return res
}

func (v *MluaVisitor) VisitBinaryAddSubExpression(ctx *parser.BinaryAddSubExpressionContext) string {
	left := v.VisitExpressionNode(ctx.GetLeft())
	leftInt, err := strconv.Atoi(left)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	right := v.VisitExpressionNode(ctx.GetRight())
	rightInt, err := strconv.Atoi(right)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	operator := ctx.GetOperator().GetText()
	var res string
	switch operator {
	case "+":
		res = strconv.Itoa(leftInt + rightInt)
	case "-":
		res = strconv.Itoa(leftInt - rightInt)
	}
	return res
}

func (v *MluaVisitor) VisitBinaryComparisonExpression(ctx *parser.BinaryComparisonExpressionContext) string {
	left := v.VisitExpressionNode(ctx.GetLeft())
	leftInt, err := strconv.Atoi(left)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	right := v.VisitExpressionNode(ctx.GetRight())
	rightInt, err := strconv.Atoi(right)
	if err != nil {
		log.Fatalf("Detect binary operatons with non-integer values")
	}
	operator := ctx.GetOperator().GetText()
	var res string
	switch operator {
	case ">":
		res = strconv.FormatBool(leftInt > rightInt)
	case ">=":
		res = strconv.FormatBool(leftInt >= rightInt)
	case "~=":
		res = strconv.FormatBool(leftInt != rightInt)
	case "==":
		res = strconv.FormatBool(leftInt == rightInt)
	case "<":
		res = strconv.FormatBool(leftInt < rightInt)
	case "<=":
		res = strconv.FormatBool(leftInt <= rightInt)
	}
	return res
}

func (v *MluaVisitor) VisitLiteralExpression(ctx *parser.LiteralExpressionContext) string {
	return ctx.GetText()
}

func (v *MluaVisitor) VisitUnaryExpression(ctx *parser.UnaryExpressionContext) string {
	temp := v.VisitExpressionNode(ctx.GetExpr())
	res, err := strconv.Atoi(temp)
	if err != nil {
		log.Fatalf("Detect unary operation with non-integer value")
	}
	return strconv.Itoa(-res)
}

func (v *MluaVisitor) VisitInputExpression(ctx *parser.InputExpressionContext) string {
	var input string
	fmt.Scanf("%s", &input)
	_, err := strconv.Atoi(input)
	if err != nil {
		if input == "true" || input == "false" {
			return input
		}
		return "nil"
	}
	return input
}
