package runtime

import (
	"fmt"

	"gitlab.com/romko11l/mulua/internal/parser"
)

func (v *MluaVisitor) VisitStatement(ctx *parser.StatementContext) interface{} {
	statement := ctx.GetChild(0)
	switch statement := statement.(type) {
	case *parser.PrintStatementContext:
		v.VisitPrintStatement(statement)
	case *parser.AssignmentStatementContext:
		v.VisitAssignmentStatement(statement)
	case *parser.IfStatementContext:
		v.VisitIfStatement(statement)
	case *parser.WhileStatementContext:
		v.VisitWhileStatement(statement)
	}
	return nil
}

func (v *MluaVisitor) VisitPrintStatement(ctx *parser.PrintStatementContext) interface{} {
	expr := ctx.GetExpr()
	fmt.Println(v.VisitExpressionNode(expr))
	return nil
}

func (v *MluaVisitor) VisitAssignmentStatement(ctx *parser.AssignmentStatementContext) interface{} {
	left := ctx.GetLeft()
	key := left.GetText()
	v.env[key] = v.VisitExpressionNode(ctx.GetRight())
	return nil
}

func (v *MluaVisitor) VisitIfStatement(ctx *parser.IfStatementContext) interface{} {
	condition := v.VisitExpressionNode(ctx.GetCondition())
	if condition != "nil" && condition != "false" {
		ifBody := ctx.GetIfbody()
		v.VisitBlockStatement(ifBody.(*parser.BlockStatementContext))
	} else {
		elseBody := ctx.GetElsebody()
		if elseBody != nil {
			v.VisitBlockStatement(elseBody.(*parser.BlockStatementContext))
		}
	}
	return nil
}

func (v *MluaVisitor) VisitWhileStatement(ctx *parser.WhileStatementContext) interface{} {
	body := ctx.GetBody()
	var condition string
	var boolCond bool
	for {
		condition = v.VisitExpressionNode(ctx.GetCondition())
		boolCond = (condition != "nil" && condition != "false")
		if !boolCond {
			break
		}
		v.VisitBlockStatement(body.(*parser.BlockStatementContext))
	}
	return nil
}
