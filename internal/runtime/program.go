package runtime

import (
	"gitlab.com/romko11l/mulua/internal/parser"
)

func (v *MluaVisitor) VisitProgram(ctx *parser.ProgramContext) interface{} {
	blockstatement := ctx.GetBlock()
	v.VisitBlockStatement(blockstatement.(*parser.BlockStatementContext))
	return nil
}
